## Policy cancellation project. Post bank

**Partner**: Post bank

**Data source**: ITM database (01.01.2018-01.10.2020)

**Developer**: Igor Vetoshev

**Date**: 28.10.2021

**Description**: The project is designed to predict policy cancellation for Post bank.

Data includes policy cancellations taken from ITM database and covers 01.01.2018-01.10.2020 period.

The size of data is 100,000 rows and 15  columns: gender, Education, Employment type, Insurace premium, Insurance amount, Loan period, etc.

The model is designed by using a Classification model of Xgboost.

The optimization metric is Logloss (Train/Val/Test).
The business metric is described in the documentation.

